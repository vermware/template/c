#!/bin/sh

PLANTUML=master

set -eu

apt-get update

apt-get dist-upgrade -y

apt-get install --no-install-recommends -y \
	ca-certificates \
	cmake \
	curl \
	default-jre-headless \
	doxygen \
	git \
	graphviz \
	make \
	python3-pip \
	python3-setuptools \
	python3-wheel

# some font metapackages use recommends to install their subpackages
apt-get install -y \
	fonts-dejavu \
	fonts-liberation \
	fonts-noto

ln -s /usr/bin/python3 /usr/local/bin/python
ln -s /usr/bin/pip3 /usr/local/bin/pip

pip install \
	breathe \
	sphinx \
	sphinx_rtd_theme \
	sphinxcontrib-plantuml

mkdir -p /opt/plantuml
for i in batik-all-1.7.jar jlatexmath-minimal-1.0.3.jar jlm_cyrillic.jar \
	jlm_greek.jar plantuml.jar
do
	curl -Lf \
		-o /opt/plantuml/$i \
		https://gitlab.mel.vin/mirror/plantuml/raw/$PLANTUML/$i
done

printf '%s\n%s\n' \
	'#!/bin/sh' \
	'exec java -jar /opt/plantuml/plantuml.jar "$@"' \
	> /usr/local/bin/plantuml
chmod +x /usr/local/bin/plantuml

apt-get purge -y \
	curl

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
