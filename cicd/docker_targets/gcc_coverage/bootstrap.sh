#!/bin/sh

DOCTEST=master

set -eu

apt-get update

apt-get dist-upgrade -y

apt-get install --no-install-recommends -y \
	binutils \
	ca-certificates \
	cmake \
	curl \
	g++ \
	gcc \
	gcovr \
	git \
	lcov \
	libc-dev \
	make

mkdir -p /usr/local/include/doctest
curl -Lf \
	-o /usr/local/include/doctest/doctest.h \
	https://gitlab.mel.vin/mirror/doctest/raw/$DOCTEST/doctest/doctest.h

apt-get purge -y \
	curl

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
