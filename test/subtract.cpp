#include <c_template/subtract.h>
#include <doctest/doctest.h>

constexpr uint8_t A = 7U;
constexpr uint8_t B = 9U;

SCENARIO("subtract two numbers")
{
	GIVEN("two numbers")
	{
		uint8_t a = A;
		REQUIRE(a == A);

		uint8_t b = B;
		REQUIRE(b == B);

		WHEN("they are subtracted from another")
		{
			int16_t sub1 = subtract(a, b);
			int16_t sub2 = subtract(b, a);

			THEN("the result is correct")
			{
				REQUIRE(sub1 == a - b);
				REQUIRE(sub2 == b - a);
			}
		}
	}
}
