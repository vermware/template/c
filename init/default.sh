#!/bin/sh

# commented variables will fallback to default values

# default: N/A
# available hardware architecture running GitLab runners. If multiple are
# available, gitlab-ci.yml may require manual editing to fit the desired jobs
# architecture setup. Examples are amd64, ppc64le, aarch64 and s390x.
# m4 quote strings [[[ and ]]] are illegal.
arch=ppc64le

# default: git config user.name if possible, else unix user name
# m4 quote strings [[[ and ]]] are illegal.
author=vermware

# default: $year, $author
# m4 quote strings [[[ and ]]] are illegal.
copyright="2016-2020, $author"

# default: __DESCRIPTION__
# m4 quote strings [[[ and ]]] are illegal.
description='Template for C'

# default: git config user.email if possible, else '__AUTHOR_MAIL__'
# m4 quote strings [[[ and ]]] are illegal.
mail=vermeeren@vermwa.re

# default: current directory name
# project must contain only [a-zA-Z0-9-_]
project=c_template

# default: '$CI_REGISTRY_IMAGE/cicd' (literal string, not shell variable)
# must not end with a trailing /
# m4 quote strings [[[ and ]]] are illegal.
#registry_url=

# default: no
# setting it to any non-empty value evaluates as yes
# m4 quote strings [[[ and ]]] are illegal.
use_gitlab_ci=yes

# default: 0.0.0
# m4 quote strings [[[ and ]]] are illegal.
version=1.12.0
