#include <c_template/subtract.h>

int16_t subtract(uint8_t a, uint8_t b)
{
	/*
	 * Old GCC versions without -Warith-conversion, before Debian bullseye,
	 * emit false warnings for -Wconversion. For details check upstream bug.
	 * https://gcc.gnu.org/bugzilla/show_bug.cgi?id=40752
	 */
	return (int16_t)(a - b);
}
