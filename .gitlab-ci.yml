stages:
  - cicd
  - analyse
  - test
  - package
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: normal
  CLANG_TOOLS: $CI_REGISTRY_IMAGE/cicd/clang_tools:latest
  COREUTILS: $CI_REGISTRY_IMAGE/cicd/coreutils:latest
  DOXYGEN: $CI_REGISTRY_IMAGE/cicd/doxygen:latest
  GCC_COVERAGE: $CI_REGISTRY_IMAGE/cicd/gcc_coverage:latest
  MINGW_W64: $CI_REGISTRY_IMAGE/cicd/mingw_w64:latest
  SPHINX_HTML: $CI_REGISTRY_IMAGE/cicd/sphinx_html:latest

default:
  tags:
    - ppc64le
    - docker
  interruptible: true

workflow:
  rules:
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_PIPELINE_SOURCE == "push"'
      when: never
    - when: always

.image/build:
  stage: cicd
  before_script:
    - |
      <<-EOF docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
      $CI_REGISTRY_PASSWORD
      EOF
  script:
    - TARGET="$(printf '%s\n' "$CI_JOB_NAME" | cut -f 2 -d /)"
    - cd "cicd/docker_targets/$TARGET"
    - docker build --pull -t "$CI_REGISTRY_IMAGE/cicd/$TARGET:latest" .
  after_script:
    - docker logout "$CI_REGISTRY"

.image:
  extends: .image/build
  environment:
    name: '$CI_JOB_NAME'
  script:
    - TARGET="$(printf '%s\n' "$CI_JOB_NAME" | cut -f 2 -d /)"
    - cd "cicd/docker_targets/$TARGET"
    - docker build --pull -t "$CI_REGISTRY_IMAGE/cicd/$TARGET:latest" .
    - docker push "$CI_REGISTRY_IMAGE/cicd/$TARGET:latest"
  resource_group: '$CI_JOB_NAME'

.image/ppc64le/build:
  extends: .image/build
  tags:
    - ppc64le
    - debian
    - vm

.image/ppc64le:
  extends: .image
  tags:
    - ppc64le
    - debian
    - vm

image/clang_tools/ppc64le/build:
  extends: .image/ppc64le/build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
      - cicd/docker_targets/clang_tools/**/*

image/clang_tools/ppc64le:
  extends: .image/ppc64le
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"
        && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - cicd/docker_targets/clang_tools/**/*
    - if: '$ENABLE_STAGE_CICD'

image/coreutils/ppc64le/build:
  extends: .image/ppc64le/build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
      - cicd/docker_targets/coreutils/**/*

image/coreutils/ppc64le:
  extends: .image/ppc64le
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"
        && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - cicd/docker_targets/coreutils/**/*
    - if: '$ENABLE_STAGE_CICD'

image/doxygen/ppc64le/build:
  extends: .image/ppc64le/build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
      - cicd/docker_targets/doxygen/**/*

image/doxygen/ppc64le:
  extends: .image/ppc64le
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"
        && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - cicd/docker_targets/doxygen/**/*
    - if: '$ENABLE_STAGE_CICD'

image/gcc_coverage/ppc64le/build:
  extends: .image/ppc64le/build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
      - cicd/docker_targets/gcc_coverage/**/*

image/gcc_coverage/ppc64le:
  extends: .image/ppc64le
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"
        && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - cicd/docker_targets/gcc_coverage/**/*
    - if: '$ENABLE_STAGE_CICD'

image/mingw_w64/ppc64le/build:
  extends: .image/ppc64le/build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
      - cicd/docker_targets/mingw_w64/**/*

image/mingw_w64/ppc64le:
  extends: .image/ppc64le
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"
        && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - cicd/docker_targets/mingw_w64/**/*
    - if: '$ENABLE_STAGE_CICD'

image/sphinx_html/ppc64le/build:
  extends: .image/ppc64le/build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
      - cicd/docker_targets/sphinx_html/**/*

image/sphinx_html/ppc64le:
  extends: .image/ppc64le
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"
        && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - cicd/docker_targets/sphinx_html/**/*
    - if: '$ENABLE_STAGE_CICD'

.analyse-if: &analyse-if
  if: '$DISABLE_STAGE_ANALYSE == null'

clang_format:
  stage: analyse
  image: $CLANG_TOOLS
  rules:
    - *analyse-if
  script:
    - mkdir build
    - cd build
    - cmake -DSRC:BOOL=OFF -DFORMAT:BOOL=ON -DWERROR:BOOL=ON ..
    - make format_check

clang_tidy/cache:
  stage: analyse
  image: $CLANG_TOOLS
  cache:
    key: 'clang_tidy'
    paths: ['build/']
  rules:
    - if: '$CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH'
      when: never
    - *analyse-if
  script:
    - mkdir -p build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE:STRING=Debug -DCLANG_TIDY:BOOL=ON -DTEST:BOOL=ON
      -DWERROR:BOOL=ON ..
    - make -j $(nproc)

clang_tidy:
  extends: clang_tidy/cache
  cache:
    key: 'clang_tidy'
    paths: ['build/']
    policy: pull
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - *analyse-if

doxygen:
  stage: analyse
  image: $DOXYGEN
  rules:
    - *analyse-if
  script:
    - mkdir build
    - cd build
    - cmake -DSRC:BOOL=OFF -DDOC:STRING=doxygen -DWERROR:BOOL=ON ..
    - make doc

init_template:
  stage: analyse
  image: $COREUTILS
  rules:
    - *analyse-if
  script:
    - ./init.sh template
    - git diff --exit-code

line_limit:
  stage: analyse
  image: $COREUTILS
  rules:
    - *analyse-if
  script:
    - mkdir build
    - cd build
    - cmake -DSRC:BOOL=OFF -DLINE_LIMIT:BOOL=ON -DWERROR:BOOL=ON ..
    - make line_limit

regex_check:
  stage: analyse
  image: $COREUTILS
  rules:
    - *analyse-if
  script:
    - mkdir build
    - cd build
    - cmake -DSRC:BOOL=OFF -DREGEX_CHECK:BOOL=ON -DWERROR:BOOL=ON ..
    - make regex_check

.test-if: &test-if
  if: '$DISABLE_STAGE_TEST == null'

clang_sanitise/cache:
  stage: test
  needs: []
  image: $CLANG_TOOLS
  cache:
    key: 'clang_sanitise'
    paths: ['build/']
  rules:
    - if: '$CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH'
      when: never
    - *test-if
  script:
    - mkdir -p build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE:STRING=Debug -DSANITISE:BOOL=ON -DTEST:BOOL=ON
      -DWERROR:BOOL=ON ..
    - make -j $(nproc)
    - ctest -j $(nproc) --output-on-failure

clang_sanitise:
  extends: clang_sanitise/cache
  cache:
    key: 'clang_sanitise'
    paths: ['build/']
    policy: pull
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - *test-if

gcc_coverage/cache:
  stage: test
  needs: []
  image: $GCC_COVERAGE
  cache:
    key: 'gcc_coverage'
    paths: ['build/']
  rules:
    - if: '$CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH'
      when: never
    - *test-if
  coverage: '/^ *lines\.+: (\d+\.\d+\%) \(\d+ of \d+ lines\)$/'
  script:
    - mkdir -p build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE:STRING=Debug -DCOVERAGE:BOOL=ON -DTEST:BOOL=ON
      -DWERROR:BOOL=ON ..
    - make -j $(nproc)
    - ctest -j $(nproc) --output-on-failure
    - make coverage_report
    - make coverage_xml
  artifacts:
    paths:
      - build/coverage_report
    reports:
      cobertura: build/coverage.xml

gcc_coverage:
  extends: gcc_coverage/cache
  cache:
    key: 'gcc_coverage'
    paths: ['build/']
    policy: pull
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - *test-if

sphinx_html/cache:
  stage: test
  needs: []
  image: $SPHINX_HTML
  cache:
    key: 'sphinx_html'
    paths: ['build/']
  rules:
    - if: '$CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH'
      when: never
    - *test-if
  script:
    - mkdir -p build
    - cd build
    - cmake -DSRC:BOOL=OFF -DDOC:STRING=html -DWERROR:BOOL=ON ..
    - make doc
  artifacts:
    paths:
      - build/doc/html

sphinx_html:
  extends: sphinx_html/cache
  cache:
    key: 'sphinx_html'
    paths: ['build/']
    policy: pull
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - *test-if

.package-if: &package-if
  if: '$DISABLE_STAGE_PACKAGE == null'

mingw_w64/amd64/cache:
  stage: package
  needs: []
  image: $MINGW_W64
  cache:
    key: 'mingw_w64_amd64'
    paths: ['build/']
  rules:
    - if: '$CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH'
      when: never
    - *package-if
  script:
    - mkdir -p build
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX=/
      -DCMAKE_TOOLCHAIN_FILE=../cmake/cross/mingw_x86_64.cmake ..
    - make -j $(nproc)
    - make DESTDIR="$PWD/../c_template_mingw_x86_64" install
  artifacts:
    paths:
        - c_template_mingw_x86_64

mingw_w64/amd64:
  extends: mingw_w64/amd64/cache
  cache:
    key: 'mingw_w64_amd64'
    paths: ['build/']
    policy: pull
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - *package-if

.review_base:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  image: $COREUTILS
  before_script:
    - mkdir -p ~/.ssh
    - |
      <<-EOF cat >~/.ssh/known_hosts
      $REVIEW_SSH_KNOWNHOSTS
      EOF
    - eval $(ssh-agent)
    - |
      <<-EOF ssh-add -q -
      $REVIEW_SSH_KEY
      EOF
  interruptible: false
  resource_group: 'review/$CI_COMMIT_REF_SLUG'

.review_rules_never: &review_rules_never
  if: >-
    $DISABLE_STAGE_DEPLOY != null || $DISABLE_STAGE_DEPLOY_REVIEW != null
    || $REVIEW_SSH_KNOWNHOSTS == null || $REVIEW_SSH_KEY == null
    || $REVIEW_SSH_DEST == null || $REVIEW_URL == null
  when: never

.review_rules_when: &review_rules_when
  if: '$CI_COMMIT_TAG == null || $ENABLE_STAGE_DEPLOY_REVIEW != null'

review:
  extends: .review_base
  needs: ["gcc_coverage", "sphinx_html"]
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: $REVIEW_URL/$CI_ENVIRONMENT_SLUG
    on_stop: review_stop
  rules:
    - *review_rules_never
    - *review_rules_when
  script:
    - mv build/doc/html public
    - mv build/coverage_report public/coverage
    - rsync -rlptHS --delete public/ -- "$REVIEW_SSH_DEST/$CI_ENVIRONMENT_SLUG"

review_stop:
  extends: .review_base
  allow_failure: true
  needs: []
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  rules:
    - *review_rules_never
    - <<: *review_rules_when
      when: manual
  script:
    - mkdir /tmp/empty
    - rsync -rlptHSf "+ /$CI_ENVIRONMENT_SLUG/***" -f 'P *' --delete /tmp/empty/
      -- "$REVIEW_SSH_DEST"
    - rmdir /tmp/empty

pages:
  stage: deploy
  needs: ["gcc_coverage", "sphinx_html"]
  environment:
    name: pages
  variables:
    GIT_STRATEGY: none
  image: $COREUTILS
  rules:
    - if: '$DISABLE_STAGE_DEPLOY || $DISABLE_STAGE_DEPLOY_PAGES'
      when: never
    - if: '$CI_COMMIT_TAG || $ENABLE_STAGE_DEPLOY_PAGES'
  script:
    - mv build/doc/html public
    - mv build/coverage_report public/coverage
  artifacts:
    paths:
      - public
  interruptible: false
  resource_group: pages
