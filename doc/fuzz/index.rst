Fuzz testing
============

.. note::
	The current version of the template has not integrated fuzzing in the
	CMake build system. This research document serves as an introduction to
	fuzz testing and also as a starting point for potential future
	implementation in the template.

Fuzzing is an automated software testing technique that involves providing
invalid, unexpected, or random data as inputs to a given target such as a
function or a program. The purpose is to monitor the target's execution for
results such as crashes, failed assertions, threads races and memory leaks.

.. contents::
	:local:

Gray-box fuzzing
----------------

Program structure-aware fuzzers are the only type of fuzzers covered in this
document, more specifically fuzzers treating their target as a gray box. It is
typically used with the purpose of reaching the highest code coverage degree
possible by keeping track of which areas of code are reached and mutating input
data accordingly.

Reuse of existing input
-----------------------

A corpus is a set of valid and invalid inputs for the target either collected by
previous fuzzing tests or provided manually. The corpus can be used to generate
even more sophisticated inputs by mutating already existing ones. Fuzzers
discussed in this document can also generate inputs from scratch or use already
existing corpus data.

Available fuzzers
-----------------

libFuzzer
~~~~~~~~~

libFuzzer is an in-process in-memory coverage-guided gray-box fuzzer. It is
linked with the target library for testing by feeding input to it via a specific
fuzzing entry point. The test driver file containing the latter is compiled to
produce a fuzzer binary. The workflow is very similar to unit testing in
template/c.

Recent versions of Clang, starting from 6.0, include libFuzzer with no extra
installation necessary. The flag ``-fsanitize=fuzzer`` *must* be used during the
compilation and linking of the fuzzer binary.

The fuzzer binary *should* also be built with `doc_cmake_sanitise` flags
enabled. It is preferable to compile it with different optimizations levels
e.g., ``-O0``, ``-O1`` and ``-O2`` to achieve diversified testing results.

Fuzzer binary
^^^^^^^^^^^^^

The fuzzer binary is the result of compiling the written test driver. It comes
with many flags predefined by libFuzzer. For a full list of  flags, run the
fuzzer binary with the flag ``-help=1``.

Many flags are interesting at first glance such as specifying the number of
individual test runs, the number of jobs to run and the timeout to which the
process will abort after reaching.

Entry point
^^^^^^^^^^^

It is a function that accepts an array of bytes and its size. The bytes of data
*should* be then provided to the target however seen most fit.

.. code-block:: c

	int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size)
	{
		/* pre-func operations... */
		ctemplate_func(data, size);
		/* post-func operations... */
		return 0; /* non-zero return values are reserved. */
	}

See also
^^^^^^^^

* https://github.com/google/fuzzing/blob/master/tutorial/libFuzzerTutorial.md
* https://llvm.org/docs/LibFuzzer.html
* https://gitlab.com/stkerr/c-cpp-fuzzing-example
