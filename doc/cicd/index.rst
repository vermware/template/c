CICD
====

The template offers pre-configured GitLab CI/CD yaml file ``.gitlab-ci.yml``.

Gitlab Runner
-------------

The GitLab Runner is managed by the system administrator and not the template
itself. The first one, tagged as ``vm``, is tasked with building and pushing
docker images, it requires a POSIX-compliant environment with coreutils
and docker installed. The second one, tagged as ``docker``, is a docker
executor runner.

Stages
------

A stage is defined by a set of jobs. Jobs following the form ``job:arch`` are
limited to a specific target architecture. The following stages are defined.

.. _cicd_stage_cicd:

CICD
^^^^

Docker images are built and pushed to GitLab's container registry during
this stage. By default, jobs are enabled only if changes were introduced
to their respective folders under ``cicd/docker_targets``. It can be also
force-enabled by setting the variable ``ENABLE_STAGE_CICD``.

Analyse
^^^^^^^

Jobs in this stage primarily consist of running static analysis tools and
utilities for policy enforcement. All jobs in this stage are allowed to
fail, in such a case the pipeline will "pass with warnings", as GitLab puts
it. It's enabled by default and can be disabled by setting the variable
``DISABLE_STAGE_ANALYSE``. Its jobs are defined as the following.

* ``clang_format``
* ``clang_tidy``
* ``doxygen``
* ``line_limit``
* ``regex_check``

Test
^^^^

Several reports artifacts are produced during this stage, this includes
sanitisation and test coverage reports as well as HTML documentation
generation. It's enabled by default and can be disabled by setting the variable
``DISABLE_STAGE_TEST``. Its jobs are defined as the following.

* ``clang_sanitise``
* ``gcc_coverage``
* ``sphinx_html``

Package
^^^^^^^

The stage compiles and packages the project's sources into platform and
architecture-specific artifacts. It's enabled by default and can be disabled
by setting the variable ``DISABLE_STAGE_PACKAGE``. Its jobs are defined as
the following.

* ``mingw_w64:amd64``

Deploy
^^^^^^

This stage deploys per-branch documentation review and deploys the latest
tag's documentation to GitLab pages. It can be disabled by setting the
variable ``DISABLE_STAGE_DEPLOY``. Its jobs can be force-enabled or disabled
by setting their respective variables ``ENABLE_STAGE_DEPLOY_REVIEW``,
``ENABLE_STAGE_DEPLOY_PAGES``, ``DISABLE_STAGE_DEPLOY_REVIEW`` and
``DISABLE_STAGE_DEPLOY_PAGES``. They are defined as the following.

* ``review``
	* ``review_stop``
* ``pages``

The ``review`` job will automatically be skipped if the configuration is
incomplete.

.. _cicd_variables:

Variables
---------

Several variables are introduced to configure how the pipelines *should* work.
These are listed in :numref:`cicd_var_index`.

CICD pipelines utilize a directed acyclic graph to build dependency
relationships between jobs. If a job depends on other jobs from different stages
that are disabled, a YAML error will occur. This will result in pipeline failure
due to the following upstream limitation (GitLab 12.8.1 at the time of writing):

	"If needs: is set to point to a job that is not instantiated because
	of only/except rules or otherwise does not exist, the pipeline
	will be created with YAML error."

	-- https://docs.gitlab.com/ee/ci/yaml/#requirements-and-limitations

For an improved DAG-based parallel jobs execution, most jobs are executed
independently from analyse stage jobs by setting ``needs`` to an empty array.
However, this causes them to fail in the case when related CICD jobs are also
executed by the pipeline. The user is therefore required to manually run the
pipeline a second time after the images have been successfully built.

For a sequential jobs execution with support for ``DISABLE_STAGE_*``, the user
has to manually edit ``.gitlab-ci.yml`` to remove ``needs``, replacing it with
``dependencies`` where needed.

.. list-table:: Variables index
	:header-rows: 1
	:name: cicd_var_index

	* * Variable
	  * Definition
	* * ``ENABLE_STAGE_CICD``
	  * Enable the execution of CICD jobs.
	* * ``ENABLE_STAGE_DEPLOY_REVIEW``
	  * Enable the execution of deploy review job.
	* * ``ENABLE_STAGE_DEPLOY_PAGES``
	  * Enable the execution of deploy pages job.
	* * ``DISABLE_STAGE_ANALYSE``
	  * Disable the execution of analyse jobs.
	* * ``DISABLE_STAGE_TEST``
	  * Disable the execution of test jobs.
	* * ``DISABLE_STAGE_PACKAGE``
	  * Disable the execution of package jobs.
	* * ``DISABLE_STAGE_DEPLOY``
	  * Disable the execution of deploy jobs.
	* * ``DISABLE_STAGE_DEPLOY_REVIEW``
	  * Disable the execution of deploy review job.
	* * ``DISABLE_STAGE_DEPLOY_PAGES``
	  * Disable the execution of deploy pages job.
