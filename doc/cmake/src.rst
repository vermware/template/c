src
===

When configuring with ``-DSRC:BOOL=OFF`` nothing source code-related will be
configured. This is intended for building documentation without requiring a
compiler to be installed. Used in CI for various jobs.
