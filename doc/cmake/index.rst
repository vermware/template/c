.. _cmake:

CMake build system
==================

Options
-------

Below is a list of all possible options that can be configured in CMake. Their
effect on the build and integrations with other options is also described.

.. toctree::
	:caption: Index
	:glob:

	*

In case you haven't used CMake before, there are three tools to configure:

* ``cmake``

	* command-line interface, have to specify all options at once

* ``ccmake``

	* curses interface, can see the options and select them

* ``cmake-gui``

	* Qt interface, can see the options and select them

Typically an interactive interface is used, unless CMake is being used in a
script or similar automated style. The example in :numref:`cmake_build_example`
uses ``ccmake`` in a Unix-like environment. Using ``cmake-gui`` is also
possible, no command line is needed in that case.

.. code-block:: console
	:name: cmake_build_example
	:caption: Configuration example using ``ccmake``

	# the current directory must be the root of the repo
	mkdir build
	cd build

	# configure, set desired options, configure and generate
	ccmake ..

	# from here on it depends on the generator used
	# below example is for the "Unix Makefiles" generator
	make help

Downstream
----------

A ``<project>Config.cmake`` file is provided for downstream projects that wish
to use this project. These files are generated when the ``install`` target is
called. Downstream projects can then use the standard ``find_package()``
functionality and link to the provided targets.
