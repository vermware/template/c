install
=======

``install`` is one of the default targets added by CMake, similar to ``all`` and
``clean``. The ``install`` target will install the configured binaries and other
meta-data files to a given directory. This project makes use of the
``GNUInstallDirs`` CMake module. This module provides a set of standard
installation directories. These are all in the form of ``CMAKE_INSTALL_<dir>``.

When configuring the build directory it is possible to set these directories to
any preferred location in the same way as other variables can be configured.  If
no value is defined then the default values will be used, e.g. ``include`` for
``CMAKE_INSTALL_INCLUDEDIR``. See
https://cmake.org/cmake/help/latest/module/GNUInstallDirs.html for the full list
of directories.
